package com.example.gateway;

import java.io.Console;
import java.util.Base64;
import java.util.Map;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;
import com.google.gson.Gson;
import reactor.core.publisher.Mono;

@Service
public class RequestBodyRewrite implements RewriteFunction<String, String> {
    @Override
    public Publisher<String> apply(ServerWebExchange exchange, String body) {
        Gson gson = new Gson();
        try {
            Map<String, Object> map = gson.fromJson(body, Map.class);
            String encoded = map.get("test").toString();
            byte[] decoded = Base64.getDecoder().decode(encoded);
            map.put("test", new String(decoded, "UTF-8"));
            return Mono.just(gson.toJson(map, Map.class));
        } catch (Exception ex) {

            // Throw custom exception here
            throw new RuntimeException(
                    "An error occured while transforming the request body in class RequestBodyRewrite.");
        }
    }
}